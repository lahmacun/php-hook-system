<?php

$hooks = array();

function perform_action() {
    global $hooks;
    $args = func_get_args();
    $hook_name = array_shift($args);
    if(!isset($hooks[$hook_name]))
        return ["error" => "Hook Not Added"];

    if(!file_exists("plugins/".$hook_name.".php"))
        return ["error" => "Plugin Not Found"];

    include "plugins/".$hook_name.".php";

    foreach($hooks[$hook_name] as $function) {
        echo $function;
        $function($args);
    }

}

function add_action($hook_name, $hook_function) {
    global $hooks;
    $hooks[$hook_name][] = $hook_function;
}

add_action("count", "countUp");

perform_action("count", 0, 10);